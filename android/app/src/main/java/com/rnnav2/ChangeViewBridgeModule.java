package com.rnnav2;


import android.app.Activity;
import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import io.flutter.embedding.android.FlutterActivity;

public class ChangeViewBridgeModule extends ReactContextBaseJavaModule {

    @Override
    public String getName() {
        return "ChangeViewBridge";
    }

    ChangeViewBridgeModule(ReactApplicationContext context) {
        super(context);
    }

    @ReactMethod
    public void changeToNativeView() {


        Activity currentActivity = getReactApplicationContext().getCurrentActivity();
        Intent intent = FlutterActivity
                .withCachedEngine("flutter_module")
                .build(currentActivity);

        currentActivity.startActivity(intent);

    }
}
