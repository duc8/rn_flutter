#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>

@import Flutter;

@interface AppDelegate : FlutterAppDelegate <RCTBridgeDelegate>

@property (nonatomic,strong) FlutterEngine *flutterEngine;

- (void)showFlutter;

@end
